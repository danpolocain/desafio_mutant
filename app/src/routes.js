const userController = require('./controllers/usersController')
const routes = require('express').Router()
const fs = require('fs')




routes.get('/users/sites', userController.usersSites)
routes.get('/users/ordered',userController.usersOrdered)
routes.get('/users/suite',userController.addressContains) 

module.exports = routes