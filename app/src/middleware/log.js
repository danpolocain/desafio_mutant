const axios = require('axios')

module.exports.log = function (log){
    axios.post('localhost:9200', log)
        .then(resp => console.log(resp))
        .error(err => console.log(err))
}