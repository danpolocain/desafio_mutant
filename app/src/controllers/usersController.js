const axios = require('axios')

exports.usersSites = (req, res, next) => {
    axios.get('https://jsonplaceholder.typicode.com/users')
        .then((response) => {
            let sites = []
            response.data.map((data) => {
                sites.push(data.website)
            })
            res.send(200, sites)
        })
        .catch(err => {
            console.log()
            res.status(err.response.status).send(err.response.text)
        })
}

exports.usersOrdered = (req, res, next) => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                let usersArray = []
                let userObj;
    
                response.data.map((user) => {
                const id = user.id
                const name = user.name
                const email = user.email
                const company = user.company
    
                userObj = {
                    id,
                    name,
                    email,
                    company
                }
                usersArray.push(userObj)
    
                })
                const sorted = usersArray.sort((a,b) => {
                    const nameA = a.name.toLowerCase()
                    const nameB = b.name.toLowerCase()
    
                    if(nameA <  nameB) {
                    return -1
                    } else if(nameA > nameB) {
                    return 1
                    } else {
                    return 0
                    }
                
                })
                res.send(200, {
                    sorted
                })
            }
            )
            .catch(err => res.send(500, err))
}

exports.addressContains = (req, res, next) => {
    axios.get('https://jsonplaceholder.typicode.com/users')
        .then((response) => {
            const suites =[]
            response.data.map((data) => {
                
                const words = lo.words()
                if(data.address.suite.indexOf('Suite') >= 0){

                    suites.push(data)
                }
                
            })
            res.send(200, suites)
        })
        .catch(err => {
            res.status(err.response.status).send(err.response.text)
        })
}